require("dotenv").load();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require("passport");

require('./app_api/models/db');
require("./app_api/config/passport");

//var routes = require('./app_server/routes/index');
var routesApi = require('./app_api/routes/index');

var app = express();

// view engine setup
//app.set('views', path.join(__dirname, 'app_server', 'views'));
//app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));     // сторонние библиотеки
app.use(express.static(path.join(__dirname, 'app_client'))); // всё приложение Angular

app.use(passport.initialize());

// app.use('/', routes);
app.use('/api', routesApi);

app.use(function(req, res) { // выкидываем основную страницу в браузер
    res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
}); // она подцепляет все свои зависимости

// error handlers

// Обработчик для ошибки "не авторизовано"
app.use(function (err, req, res, next) {
    if (err.name === "UnauthorizedError") {
        res.status(401);
        res.json({"message" : err.name + ": " + err.message});
    }
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
