var mongoose = require('mongoose');
var readLine = require('readline');

var dbURI = 'mongodb://localhost/Repa';
mongoose.connect(dbURI, { useNewUrlParser: true });

if (process.platform === "win32"){
    var r1 = readLine.createInterface ({
        input: process.stdin,
        output: process.stdout
    });
    r1.on("SIGINT", () => {
        process.emit("SIGINT");
    });
}

var gracefulShutdown = (msg, callback) => { // функция тихого закрытия
    mongoose.connection.close(() => {
        console.log('Mongoose disconnected through ' + msg); // сообщение
        callback();                         // какая-либо каллбэк функция
    });
};

mongoose.connection.on('connected', () => { // подключились
    console.log('Mongoose connected to ' + dbURI);
});

mongoose.connection.on('error', (err) => { // возникла ошибка
    console.log('Mongoose connected error ' + err);
});

mongoose.connection.on('disconnected', () => { // отключились
    console.log('Mongoose disconnected');
});

process.once('SIGUSR2', () => { // для сигнала от nodemon
    gracefulShutdown('nodemon restart', () => {
        process.kill(process.pid, 'SIGUSR2');
    });
});

process.on('SIGINT', () => {    // для сигнала от node
    gracefulShutdown('app termination', () => {
        process.exit(0);
    });
});

process.on('SIGTERM', () => {   // для сигнала от Heroku
    gracefulShutdown('Heroku app shutdown', () => {
        process.exit(0);
    });
});

require('./rates');
require('./users');