var mongoose = require('mongoose');

var ratesSchema = new mongoose.Schema({
    title: {type: String, required: true},
    Description: {type: String, required: true},
    cost: {type: Number, required: true},
    createdOn: {type: Date, "default": Date.now}
});

mongoose.model('rates', ratesSchema);