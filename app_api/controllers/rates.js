var mongoose = require('mongoose');
var RatesMongoose = mongoose.model('rates');

var sendJsonResponse = function (res, status, content) {
    res.status(status);
    res.json(content);
};

module.exports.ratesList = function (req, res) {
    RatesMongoose
        .find()
        .exec(function (err, rates) {

            if (!rates) {
                sendJsonResponse(res, 404, {
                    "message": "rates not found"
                });
                return;
            } else if (err) {
                sendJsonResponse(res, 404, err);
                return;
            }

            var ratesList = [];
            rates.forEach((doc) => {
                ratesList.push({
                    title: doc.title,
                    cost: doc.cost,
                    Description: doc.Description,
                    createdOn: doc.createdOn,
                    _id: doc._id
                });
            });
            sendJsonResponse(res, 200, ratesList);
        });
};

module.exports.ratesCreate = function (req, res) {
    RatesMongoose.create({
        title: req.body.title,
        Description: req.body.Description,
        cost: req.body.cost
    }, (err, rate) => {
        if (err) {
            sendJsonResponse(res, 400, err);
        } else {
            sendJsonResponse(res, 201, rate);
        }
    });
};

module.exports.ratesReadOne = function (req, res) {
    if (req.params && req.params.rateid) {
        RatesMongoose
            .findById(req.params.rateid)
            .exec(function (err, rate) {

                if (!rate) {
                    sendJsonResponse(res, 404, {
                        "message": "rateid not found"
                    });
                    return;
                } else if (err) {
                    sendJsonResponse(res, 404, err);
                    return;
                }

                sendJsonResponse(res, 200, rate);
            });
    } else {
        sendJsonResponse(res, 404, {
            "message": "No rateid in request"
        });
    }
};

module.exports.ratesUpdateOne = function (req, res) {
    if (!req.params.rateid) {
        sendJsonResponse(res, 404, {
            "message": "Not found, rateid is required"
        });
        return;
    }

    RatesMongoose
        .findById(req.params.rateid)
        .select()
        .exec(function (err, rate) {
            if (!rate) {
                sendJsonResponse(res, 404, {
                    "message": "rateid not found"
                });
                return;
            } else if (err) {
                sendJsonResponse(res, 404, err);
                return;
            }

            rate.title = req.body.title,
            rate.Description = req.body.Description,
            rate.cost = req.body.cost;

            rate.save((err, rate) => {
                if (err) {
                    sendJsonResponse(res, 400, err);
                } else {
                    sendJsonResponse(res, 200, rate);
                }
            });
        });
};

module.exports.ratesDeleteOne = function (req, res) {
    var rateid = req.params.rateid;
    if (rateid) {
        RatesMongoose
            .findByIdAndRemove(rateid)
            .exec(function (err, rate) {
                if (err) {
                    sendJsonResponse(res, 404, err);
                    return;
                }
                sendJsonResponse(res, 204, null);
            });
    } else {
        sendJsonResponse(res, 404, {
            "message": "No rateid"
        });
    }
};