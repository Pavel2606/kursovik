var express = require('express');
var router = express.Router();
var jwt = require("express-jwt");
var auth = jwt({
    secret: process.env.JWT_SECRET,
    userProperty: "payload"
});

var ctrlRates = require('../controllers/rates');
var ctrlAuth = require("../controllers/authentication");

// Тарифы
router.get('/rates', ctrlRates.ratesList);
router.post('/rates', ctrlRates.ratesCreate);
router.get('/rates/:rateid', ctrlRates.ratesReadOne);
router.put('/rates/:rateid', ctrlRates.ratesUpdateOne);
router.delete('/rates/:rateid', ctrlRates.ratesDeleteOne);

// Аутентификация
router.post("/register", ctrlAuth.register);
router.post("/login", ctrlAuth.login);

module.exports = router;