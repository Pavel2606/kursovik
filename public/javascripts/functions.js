function isCancelModal(res) {
    if (['cancel', 'backdrop click', 'escape key press'].indexOf(res) === -1) {
        throw res;
    }
}

if (!String.prototype.splice) {
    String.prototype.splice = function(start, delCount, newSubStr) {
        return this.slice(0, start) + newSubStr + this.slice(start + Math.abs(delCount));
    };
}