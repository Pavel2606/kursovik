'use strict';

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

describe('Тесты авторизации', function () {

    it('Должна открыться страница /login при нажатии на кнопку входа.', function () {
        browser.get('/');
        element(by.css('ul.nav:nth-child(2) > li:nth-child(1) > a:nth-child(1)')).click();
        expect(browser.getCurrentUrl()).toMatch("/login");
    });

    it('Вход пользователем repa@mail.ru должен быть успешен.', function () {
        element(by.model('vm.credentials.email')).clear().sendKeys('repa@mail.ru');
        element(by.model('vm.credentials.password')).clear().sendKeys('123456');
        element(by.css('.btn')).click();
        expect(browser.getCurrentUrl()).toMatch('/');
    });

    it('Выход должен быть успешен.', function () {
        element(by.css(".dropdown-toggle")).click();
        element(by.css(".dropdown-menu > li:nth-child(1) > a:nth-child(1)")).click();
        expect(browser.getCurrentUrl()).toMatch("/");
    });

});