### Для запуска приложения
Выполните:
1. `git clone https://gitlab.com/Pavel2606/kursovik.git`
2. `npm install`
3. `npm start`

### Для запуска e2e тестов:
1. Откройте первый терминал
2. Выполните npm run protprepare
3. Откройте второй терминал
4. Выполните npm start
5. Откройте третий терминал
6. Выполните npm run protractor'