// var request = require('request');
// var apiOptions = {
//     server: "http://localhost:3000"
// };
//
//
// var _showError = function (req, res, status) {
//     var title, content;
//     if (status === 404) {
//         title = "404, page not found";
//         content = "Oh dear. Looks like we can't find this page. Sorry";
//     } else {
//         title = status + ", something's gone wrong";
//         content = "Something, somewhere, has gone just a little bit wrong.";
//     }
//
//     res.status(status);
//     res.render('generic-text', {
//         title: title,
//         content: content
//     });
// };
//
// var _formatDistance = function (distance) {
//     var numDistance, unit;
//     if (distance > 1) {
//         numDistance = parseFloat(distance).toFixed(1);
//         unit = 'km';
//     } else {
//         numDistance = parseInt(distance * 1000, 10);
//         unit = 'm';
//     }
//     return numDistance + unit;
// };
//
// // request(requestOptions, (err, response, body) => {
// //     if (err) {
// //         console.log(err);
// //     } else if (response.statusCode === 200) {
// //         console.log(body);
// //     } else {
// //         console.log(response.statusCode);
// //     }
// // });
//
// var getLocationInfo = function (req, res, callback) {
//     var requestOptions, path;
//     path = "/api/news/" + req.params.newsid;
//     requestOptions = {
//         url: apiOptions.server + path,
//         method: "GET",
//         json: {}
//     };
//
//     request(requestOptions, (err, response, body) => {
//         // var data = body;
//         if (response.statusCode === 200) {
//             // data.coords = {
//             //     lng : body.coords[0],
//             //     lat : body.coords[1]
//             // };
//             callback(req, res, body); // renderNewsInfoPage(req, res, data)
//         } else {
//             _showError(req, res, response.statusCode);
//         }
//     });
// };
//
// var renderHomepage = function (req, res) {
//     res.render('news', {
//         title: 'Repa - find a place to work with wifi',
//         pageHeader: {
//             title: 'Repa',
//             strapline: 'Find places to work with wifi near you!'
//         },
//         sidebar: "blah-blah-blah"
//     });
// };
//
// // GET home page
// module.exports.home = function (req, res) {
//     renderHomepage(req, res);
// };
//
// var renderNewsInfoPage = function (req, res, newsDetail) {
//     res.render('news-info', {
//         title: newsDetail.name,
//         pageHeader: {title: newsDetail.name},
//         sidebar: {
//             context: 'Тут вас научат тому то и тому то. Помимо этого вы узнаете что то и то то',
//             callToAction: 'Если вам понравилось или не понравилось это, то оставьте свой отзыв. Помогите другим людям!'
//         },
//         news: newsDetail
//     });
// };
//
// // GET News info page
// module.exports.newsinfo = function (req, res) {
//     getLocationInfo(req, res, (req, res, responseData) => {
//         renderNewsInfoPage(req, res, responseData);
//     });
// };
//
// var renderCommentForm = function (req, res, newsDetail) {
//     res.render('news-comment-form', {
//         title: "Review " + newsDetail.name + " on Repa",
//         pageHeader: {title: "Review " + newsDetail.name},
//         error: req.query.err,
//         url: req.originalUrl
//     });
// };
//
// // GET News info page
// module.exports.addComment = function (req, res) {
//     getLocationInfo(req, res, (req, res, responseData) => {
//         renderCommentForm(req, res, responseData);
//     });
// };
//
// module.exports.doAddComment = function (req, res) {
//     var requestOptions, path, newsid, postdata;
//     newsid = req.params.newsid;
//     path = "/api/news/" + newsid + "/reviews";
//     postdata = {
//         author: req.body.name,
//         rating: parseInt(req.body.rating, 10),
//         reviewText: req.body.review
//     };
//     requestOptions = {
//         url: apiOptions.server + path,
//         method: "POST",
//         json: postdata
//     };
//
//     if (!postdata.author || !postdata.rating || !postdata.reviewText) {
//         res.redirect("/news/" + newsid + "/review/new?err=val");
//     } else {
//         request(requestOptions, (err, response, body) => {
//             if (response.statusCode === 201) {
//                 res.redirect("/newsinfo/" + newsid);
//             } else if (response.statusCode === 400 && body.name && body.name === "ValidationError") {
//                 res.redirect("/newsinfo/" + newsid + "/review/newComment?err=val");
//             } else {
//                 console.log(body);
//                 _showError(req, res, response.statusCode);
//             }
//         });
//     }
// };