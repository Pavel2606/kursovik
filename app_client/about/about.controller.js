(function () {

    angular
        .module("Repa_App")
        .controller("aboutCtrl", aboutCtrl);

    function aboutCtrl() {
        var vm = this;

        vm.pageHeader = {
            title: "О нашей компании",
        };
        vm.main = {
            content: "Основной задачей \"Домоуправляющей Компании Советского района\" с момента её образования являлось улучшение качества обслуживания и условий проживания нижегородцев. В 2013 году компания существенно изменила порядок работы с жителями – созданы и успешно функционируют единые центры обслуживания населения. Посетив любой из центров, вы сможете напрямую обратиться в Домоуправляющую Компанию, которая, в свою очередь, дает распоряжения подрядчикам. В новых центрах инспекторы отдела по работе с населением принимают заявления по текущему ремонту, ведут прием паспортисты (выдают справки и выписки из документов) и бухгалтеры – проводят перерасчет по квартплате. Местоположение и график работы единых центров обслуживания населения вы можете уточнить в разделе «Приём граждан»."
        };
    }
})();