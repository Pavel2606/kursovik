(function () {

    angular
        .module("Repa_App")
        .controller("editRateModalCtrl", editRateModalCtrl);

    function editRateModalCtrl($modalInstance, ratesData, rateData) {
        var vm = this;
        vm.rateData = rateData;

        vm.onSubmit = function () {
            vm.formError = "";
            if (!vm.rateData.title || !vm.rateData.Description || !vm.rateData.cost) {
                vm.formError = "Требуется заполнить все поля, пожалуйста попробуйте снова!";
                return false;
            } else {
                vm.doEditRate(vm.rateData);
                return false;
            }
        };

        vm.doEditRate = function (formData) {
            ratesData.editRate(formData.rateid, {
                title: formData.title,
                cost: formData.cost,
                Description: formData.Description
            }).then(function (res) {
                    vm.modal.close(res.data);
                }, function (res) {
                    if (res.data && res.data.length > 0) {
                        vm.formError = res.data;
                    }
                    else {
                        vm.formError = "Ваш тариф не был изменён, попробуйте снова!";
                    }
                }
            );
            return false;
        };

        vm.modal = {
            close: function (result) {
                $modalInstance.close(result);
            },
            cancel: function () {
                $modalInstance.dismiss("cancel");
            }
        };
    }

})();