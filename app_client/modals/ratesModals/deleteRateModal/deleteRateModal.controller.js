(function () {

    angular
        .module("Repa_App")
        .controller("deleteRateModalCtrl", deleteRateModalCtrl);

    function deleteRateModalCtrl($modalInstance, ratesData, rateData) {
        var vm = this;
        vm.rateData = rateData;

        vm.onSubmit = function () {
            vm.doDeleteRate();
            return false;
        };

        vm.doDeleteRate = function () {
            ratesData.deleteRate(vm.rateData.rateid)
                .then(function (res) {
                    vm.modal.close(res);
                }, function (res) {
                    if (res.data && res.data.length > 0) {
                        vm.formError = res.data;
                    }
                    else {
                        vm.formError = "Ваш тариф не был удалён, попробуйте снова!";
                    }
                });
            return false;
        };

        vm.modal = {
            close: function (result) {
                $modalInstance.close(result);
            },
            cancel: function () {
                $modalInstance.dismiss("cancel");
            }
        };
    }

})();