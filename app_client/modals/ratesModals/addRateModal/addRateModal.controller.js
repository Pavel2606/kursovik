(function () {

    angular
        .module("Repa_App")
        .controller("addRateModalCtrl", addRateModalCtrl);

    function addRateModalCtrl($modalInstance, ratesData) {
        var vm = this;

        vm.onSubmit = function () {
            vm.formError = "";
            if (!vm.formData.title || !vm.formData.Description || !vm.formData.cost) {
                vm.formError = "Требуется заполнить все поля, пожалуйста попробуйте снова!";
                return false;
            } else {
                vm.doAddRate(vm.formData);
                return false;
            }
        };

        vm.doAddRate = function (formData) {
            ratesData.addRate({
                title: formData.title,
                Description: formData.Description,
                cost: formData.cost
            }).then(function (res) {
                    vm.modal.close(res.data);
                }, function (res) {
                    if (res.data && res.data.length > 0) {
                        vm.formError = res.data;
                    }
                    else {
                        vm.formError = "Ваш тариф не был добавлен, попробуйте снова!";
                    }
                }
            );
            return false;
        };

        vm.modal = {
            close: function (result) {
                $modalInstance.close(result);
            },
            cancel: function () {
                $modalInstance.dismiss("cancel");
            }
        };
    }

})();