﻿(function () {

    angular
        .module("Repa_App")
        .controller("homeCtrl", homeCtrl);

    function homeCtrl($scope, ratesData, $window, $modal, authentication) {
        var vm = this;

        vm.isLoggedIn = authentication.isLoggedIn();

        vm.pageHeader = {
            title: "Домоуправляющая компания \"Чистота\""
        };

        ratesData.rates().then(function (res) {
            vm.message = res.data.length > 0 ? "" : "Тарифов нет пока!";
            vm.data = {ratesList: res.data};
        }, function (res) {
            vm.message = "извините, что-то не так";
        });

        vm.showError = function (error) {
            $scope.$apply(function () {
                vm.message = error.message;
            });
        };

        vm.popupAddRateForm = function () {
            var modalInstance = $modal.open({
                templateUrl: "/modals/ratesModals/addRateModal/addRateModal.view.html",
                controller: "addRateModalCtrl",
                controllerAs: "vm"
            });

            modalInstance.result.then( function (data) {
                vm.data.ratesList.push(data);
            }, function (res) {
                isCancelModal(res);
            });
        };

        vm.popupEditRateForm = function (rate) {
            var modalInstance = $modal.open({
                templateUrl: "/modals/ratesModals/editRateModal/editRateModal.view.html",
                controller: "editRateModalCtrl",
                controllerAs: "vm",
                resolve: {
                    rateData : function () {
                        return {
                            rateid : rate._id,
                            title : rate.title,
                            titlet : rate.title,
                            cost: rate.cost,
                            Description : rate.Description
                        };
                    }
                }
            });

            modalInstance.result.then( function () {
                $window.location.reload();
            }, function (res) {
                isCancelModal(res);
            });
        };

        vm.popupDeleteRateForm = function (rateid, title) {
            var modalInstance = $modal.open({
                templateUrl: "/modals/ratesModals/deleteRateModal/deleteRateModal.view.html",
                controller: "deleteRateModalCtrl",
                controllerAs: "vm",
                resolve: {
                    rateData : function () {
                        return {
                            rateid : rateid,
                            title : title
                        };
                    }
                }
            });

            modalInstance.result.then( function () {
                $window.location.reload();
            }, function (res) {
                isCancelModal(res);
            });
        };
    }
})();