(function () {

angular
    .module("Repa_App")
    .service("authentication", authentication);

function authentication($http, $window) {
    var saveToken = function (token) {
        $window.localStorage["repa-token"] = token;
    };

    var getToken = function () {
        return $window.localStorage["repa-token"];
    };

    var isLoggedIn = function() {
        var token = getToken();

        if(token) {
            var payload = JSON.parse($window.atob(token.split(".")[1]));

            return payload.exp > Date.now() / 1000;
        } else {
            return false;
        }
    };

    var currentUser = function() {
        if (isLoggedIn()) {
            var token = getToken();
            var payload = JSON.parse($window.atob(token.split(".")[1]));

            return {
                email : payload.email,
                name : payload.name
            };
        }
    };

    register = function (user) {
        return $http.post("/api/register", user).then(function (res) {
            saveToken(res.data.token);
        });
    };

    login = function (user) {
        return $http.post("/api/login", user).then(function (res) {
            saveToken(res.data.token);

        });
    };

    logout = function () {
        $window.localStorage.removeItem("repa-token");
    };

    return {
        saveToken : saveToken,
        getToken : getToken,
        isLoggedIn : isLoggedIn,
        currentUser : currentUser,
        register : register,
        login : login,
        logout : logout
    };
}

})();