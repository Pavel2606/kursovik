﻿(function () {

    angular
        .module("Repa_App")
        .service("ratesData", ratesData);

    function ratesData($http) {
        var rates = function () {
            return $http.get("/api/rates");
        };

        var rateById = function (rateid) {
            return $http.get("/api/rates/" + rateid);
        };

        var addRate = function (data) {
            return $http.post("/api/rates", data);
        };

        var editRate = function (rateid, data) {
            return $http.put("/api/rates/" + rateid, data);
        };

        var deleteRate = function (rateid) {
            return $http.delete("/api/rates/" + rateid);
        };

        return {
            rates: rates,
            rateById: rateById,
            addRate: addRate,
            editRate: editRate,
            deleteRate: deleteRate
        };
    }

})();