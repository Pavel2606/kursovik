(function () {

    angular
        .module('Repa_App')
        .controller('navigationCtrl', navigationCtrl);

    function navigationCtrl($window, $location, authentication) {
        var vm = this;

        vm.currentPath = $location.path();
        vm.isLoggedIn = authentication.isLoggedIn();
        vm.currentUser = authentication.currentUser();

        vm.logout = function () {
            authentication.logout();
            $location.path("/");
            $window.location.reload();
        };
    }

})();