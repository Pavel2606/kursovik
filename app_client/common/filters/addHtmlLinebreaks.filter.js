(function () {

    angular
        .module("Repa_App")
        .filter("addHtmlLineBreaks", addHtmlLineBreaks);

    function addHtmlLineBreaks() {
        return function (text) {
            text = text.replace(/\n/g, "<br/>");
            return text.toString();
        };
    }

})();