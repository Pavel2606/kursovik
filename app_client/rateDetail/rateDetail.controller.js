(function () {

    angular
        .module("Repa_App")
        .controller("rateDetailCtrl", rateDetailCtrl);

    function rateDetailCtrl($routeParams, ratesData) {
        var vm = this;

        vm.rateid = $routeParams.rateid;

        ratesData.rateById(vm.rateid).then(function (res) {
            vm.data = {rate: res.data};
            vm.pageHeader = {
                title: vm.data.rate.title
            };
        }, function (res) {
            console.log(res.error);
        });
    }

})();