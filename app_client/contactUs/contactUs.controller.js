(function () {

    angular
        .module("Repa_App")
        .controller("contactUsCtrl", contactUsCtrl);

    function contactUsCtrl() {
        var vm = this;

        vm.pageHeader = {
            title: "Связаться с нами быстро и легко!"
        };
        vm.main = {
            content: "Напишите нам по почте: pasha.sokolov2000@yandex.ru.\n или \n Позвоните нам по телефону: 8(800)555-35-35"
        };
    }
})();