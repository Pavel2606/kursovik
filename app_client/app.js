﻿(function () {

    angular.module("Repa_App", ["ngRoute", "ngSanitize", "ui.bootstrap"]);

    function config($routeProvider, $locationProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "/home/home.view.html",
                controller: "homeCtrl",
                controllerAs: "vm"
            })
            .when("/rate/:rateid", {
                templateUrl: "/rateDetail/rateDetail.view.html",
                controller: "rateDetailCtrl",
                controllerAs: "vm"
            })
            .when("/about", {
                templateUrl: "/common/views/genericText.view.html",
                controller: "aboutCtrl",
                controllerAs: "vm"
            })
            .when("/contactUs", {
                templateUrl: "/common/views/genericText.view.html",
                controller: "contactUsCtrl",
                controllerAs: "vm"
            })
            .when("/register", {
                templateUrl: "/auth/register/register.view.html",
                controller: "registerCtrl",
                controllerAs: "vm"
            })
            .when('/login', {
                templateUrl: '/auth/login/login.view.html',
                controller: 'loginCtrl',
                controllerAs: 'vm'
            })
            .otherwise({redirectTo: "/"});

        $locationProvider.html5Mode(true);
    }

    angular
        .module("Repa_App")
        .config(["$routeProvider", "$locationProvider", config]);
})();

