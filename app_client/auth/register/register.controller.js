(function () {

    angular
        .module("Repa_App")
        .controller("registerCtrl", registerCtrl);

    function registerCtrl($location, authentication) {
        var vm = this;

        vm.pageHeader = {
            title: "Создайте вашу учётную запись!"
        };

        vm.credentials = {
            name: "",
            email: "",
            password: ""
        };

        vm.returnPage = $location.search().page || "/";

        vm.onSubmit = function () {
            vm.formError = "";
            if (!vm.credentials.name || !vm.credentials.email || !vm.credentials.password) {
                vm.formError = "Требуется заполните все поля!";
                return false;
            } else {
                vm.doRegister();
            }
        };

        vm.doRegister = function () {
            vm.formError = "";
            authentication
                .register(vm.credentials)
                .then(function (res) {
                    $location.search('page', null);
                    $location.path(vm.returnPage);
                }, function (res) {
                    vm.formError = res.data.message;
                });
        };
    }

})();