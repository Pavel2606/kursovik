(function () {

    angular
        .module("Repa_App")
        .controller('loginCtrl', loginCtrl);

    function loginCtrl($location, authentication) {
        var vm = this;

        vm.pageHeader = {
            title: 'Зайти к нам'
        };

        vm.credentials = {
            email: "",
            password: ""
        };

        vm.returnPage = $location.search().page || '/';

        vm.onSubmit = function () {
            vm.formError = "";
            if (!vm.credentials.email || !vm.credentials.password) {
                vm.formError = "Требуется заполнить все поля!";
                return false;
            } else {
                vm.doLogin();
            }
        };

        vm.doLogin = function () {
            vm.formError = "";
            authentication
                .login(vm.credentials)
                .then(function (res) {
                    $location.search('page', null);
                    $location.path(vm.returnPage);
                }, function (res) {
                    vm.formError = res.data.message;
                });
        };
    }

})();